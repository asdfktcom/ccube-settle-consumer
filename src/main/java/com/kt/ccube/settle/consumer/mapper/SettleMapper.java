package com.kt.ccube.settle.consumer.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SettleMapper {
    void upsertContentsSummary(@Param("list") List<Map<String, Object>> list);
}
