package com.kt.ccube.settle.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;

@RestController
public class KafkaController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;	
	
	@GetMapping("/kafka/send")
	public Object execKafka(
			@RequestParam(value="topic", required=false, defaultValue="test") String topic,
			@RequestParam(value="msg", required=false, defaultValue="test") String msg) {
		kafkaTemplate.send(topic, msg);
		return ImmutableMap.<String, Object>builder()
				.put("result", "0")
				.build();
	}

}
