package com.kt.ccube.settle.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;
import com.kt.ccube.settle.consumer.service.SettleService;

@RestController
public class SettleApiController {
	
	@Autowired
	private SettleService settleService;
	
	@GetMapping("/db/init")
	public Object initTable() throws Exception {
		settleService.initSettleDb();
		return ImmutableMap.<String, Object>builder()
				.put("result", "0")
				.build();
	}
}
