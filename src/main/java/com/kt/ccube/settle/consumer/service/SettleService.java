package com.kt.ccube.settle.consumer.service;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kt.ccube.settle.consumer.mapper.SettleMapper;

@Service
public class SettleService {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private SettleMapper settleMapper;
	
	@Autowired
	private DataSource dataSource;
	
	@Value("classpath:settle_ddl.sql")
	private Resource ddlResource;
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	public void initSettleDb() throws Exception {
		
		ScriptRunner runner = new ScriptRunner(dataSource.getConnection());		
		try {
			runner.setAutoCommit(true);
			runner.setStopOnError(true);
			runner.runScript(Files.newBufferedReader(ddlResource.getFile().toPath(), StandardCharsets.UTF_8));
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
		} finally {
			runner.closeConnection();
		}

	}

    @KafkaListener(topics = "ccube")
    public void consume(String data) throws Exception {
    	log.info("## recv data from kafka : {}", data);
    	List<Map<String, Object>> listData = gson.fromJson(data, new TypeToken<List<Map<String, Object>>>(){}.getType());
    	settleMapper.upsertContentsSummary(listData);
    }

}
