
-- 콘텐츠요약기본
DROP TABLE IF EXISTS CONTS_SUMRY_BAS;
CREATE TABLE CONTS_SUMRY_BAS
(
    CONTS_ID            varchar(15)                 NOT NULL, -- 콘텐츠아이디
    FIRST_CRET_DT       timestamp                   NOT NULL DEFAULT now(), -- 최초생성일시
    FIRST_CRET_TRTR_ID  varchar(11)                 NOT NULL, -- 최초생성자아이디
    LAST_CHG_DT         timestamp                   NULL,     -- 최종변경일시
    LAST_CHG_TRTR_ID    varchar(11)                 NULL,     -- 최종변경자아이디
    CONTS_NM            varchar(255)                NULL,     -- 콘텐츠명
    BM_TYPE_ID          varchar(3)                  NULL,     -- BM유형아이디
    CONTS_TYPE_ID       varchar(3)                  NULL,     -- 콘텐츠유형아이디
    CP_ID               varchar(11)                 NULL,     -- CP아이디
    CP_NM               varchar(90)                 NULL,     -- CP명
    CONTS_CONT_NM       varchar(300)                NULL,      -- 콘텐츠계약명
    CONTS_FIRST_CRET_DT timestamp                   NULL,     -- 콘텐츠최초생성일시
    CONTS_LAST_CHG_DT   timestamp                   NULL      -- 콘텐츠최종변경일시
);
-- 콘텐츠요약기본
COMMENT ON TABLE CONTS_SUMRY_BAS IS '콘텐츠요약기본';
-- 콘텐츠아이디
COMMENT ON COLUMN CONTS_SUMRY_BAS.CONTS_ID IS '콘텐츠아이디';
-- 최초생성일시
COMMENT ON COLUMN CONTS_SUMRY_BAS.FIRST_CRET_DT IS '최초생성일시';
-- 최초생성자아이디
COMMENT ON COLUMN CONTS_SUMRY_BAS.FIRST_CRET_TRTR_ID IS '최초생성자아이디';
-- 최종변경일시
COMMENT ON COLUMN CONTS_SUMRY_BAS.LAST_CHG_DT IS '최종변경일시';
-- 최종변경자아이디
COMMENT ON COLUMN CONTS_SUMRY_BAS.LAST_CHG_TRTR_ID IS '최종변경자아이디';
-- 콘텐츠명
COMMENT ON COLUMN CONTS_SUMRY_BAS.CONTS_NM IS '콘텐츠명';
-- BM유형아이디
COMMENT ON COLUMN CONTS_SUMRY_BAS.BM_TYPE_ID IS 'BM유형아이디';
-- 콘텐츠유형아이디
COMMENT ON COLUMN CONTS_SUMRY_BAS.CONTS_TYPE_ID IS '콘텐츠유형아이디';
-- CP아이디
COMMENT ON COLUMN CONTS_SUMRY_BAS.CP_ID IS 'CP아이디';
-- CP명
COMMENT ON COLUMN CONTS_SUMRY_BAS.CP_NM IS 'CP명';
-- 콘텐츠계약명
COMMENT ON COLUMN CONTS_SUMRY_BAS.CONTS_CONT_NM IS '콘텐츠계약명';
-- 콘텐츠최초생성일시
COMMENT ON COLUMN CONTS_SUMRY_BAS.CONTS_FIRST_CRET_DT IS '콘텐츠최초생성일시';
-- 콘텐츠최종변경일시
COMMENT ON COLUMN CONTS_SUMRY_BAS.CONTS_LAST_CHG_DT IS '콘텐츠최종변경일시';
-- 콘텐츠요약기본기본키
CREATE UNIQUE INDEX PK_CONTS_SUMRY_BAS
    ON CONTS_SUMRY_BAS
    ( -- 콘텐츠요약기본
        CONTS_ID ASC -- 콘텐츠아이디
    )
;
-- 콘텐츠요약기본
ALTER TABLE CONTS_SUMRY_BAS
    ADD CONSTRAINT PK_CONTS_SUMRY_BAS
         -- 콘텐츠요약기본 기본키
    PRIMARY KEY 
    USING INDEX PK_CONTS_SUMRY_BAS;
-- 콘텐츠요약기본 기본키
COMMENT ON CONSTRAINT PK_CONTS_SUMRY_BAS ON CONTS_SUMRY_BAS IS '콘텐츠요약기본 기본키';
